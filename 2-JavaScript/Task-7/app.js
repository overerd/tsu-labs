const https = require('https'), process = require('process'), express = require('express'), morgan = require('morgan');
const app = express();
const re_datetime = /\d{4}-(?:1[02]|0\d)-(?:[0-2]\d|3[01])T[0-2]\d:[0-5]\d:[0-5]\d/, re_lead_slash = /^([^\/])/;

if (process.argv.length < 3) console.error('Usage: app.js API_KEY [SERVER_ROOT_DIR]\n\nError: no input API key');

const API_KEY = process.argv[2];
const SERVER_HOST = '127.0.0.1', SERVER_PORT = 8080, SERVER_ROOT = process.argv.length > 3 ? process.argv[3] : __dirname;
const app_config = {'lat': 35.596922, 'lon': 139.349335, 'radius': 20, 'topic_category': 292, 'key': API_KEY};

const build_request_url = (request, params) => `https://api.meetup.com${request.replace(re_lead_slash, '/$1')}?${Object.entries(params).map((_) => { return _.join('=') }).join('&')}`;

const get_list = (req, res, from, to) => {
    const params = {...app_config};

    if (from) if (!re_datetime.test(from)) from = undefined;
    if (to) if (!re_datetime.test(to)) to = undefined;

    https.get(build_request_url('/find/upcoming_events', params), (response) => {
        const body = [];

        delete params['key'];

        if (from) params['from'] = from;
        if (to) params['to'] = to;

        response.on('error', (err) => res.render('error', {'code': res.statusCode = 500, 'msg': err}));
        response.on('data', (chunk) => body.push(chunk));
        response.on('end', () => {
            const data = JSON.parse(body.join(''));

            data['events'].forEach((item) => {
                let address;

                if (item.venue) {
                    address = `${item.venue.localized_country_name}, ${item.venue.city} ${item.venue.address_1} (${item.venue.name})`
                } else if (item.group) {
                    address = `${item.group.localized_location}, ${item.group.name})`
                } else {
                    address = ''
                }

                item['full_address'] = address
            });

            params['items'] = data['events'];

            if (params['items']) {
                res.render('list', params)
            } else {
                res.render('error', {'code': res.statusCode = 500, 'msg': `Error: empty items\n\n${JSON.stringify(data)}`})
            }
        })
    });
};

app.set('view engine', 'pug');
app.set('views', './templates');

app.use('/', express.static([SERVER_ROOT, 'static'].join('/')));
app.use(morgan('dev'));

app.get('/', (req, res) => { get_list(req, res, req.query.from, req.query.to) });
app.get('/:from/:to', (req, res) => { get_list(req, res, req.params.from, req.params.to) });

app.listen(SERVER_PORT, SERVER_HOST, () => { console.log(`Server listening on ${SERVER_HOST}:${SERVER_PORT}...\n`) });
