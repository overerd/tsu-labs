from custom_lib import get_item, fetch_posts
from custom_db import PostManager

from collections import deque
from random import randint
from optparse import OptionParser

from threading import Thread, Lock

import time
import signal
import sys

usage = 'Usage: %prog [options]'

option_parser = OptionParser(usage=usage, add_help_option=False)

option_parser.add_option('-h', action='help', help='show this message and exit')

option_parser.add_option('--max-pages', dest='max_pages', metavar='INT', help='max number of pages to parse', type=int)
option_parser.add_option('--items-per-page', dest='items_per_page', metavar='INT', help='items per page', type=int)

option_parser.add_option('-w', dest='wait', metavar='SECONDS', help='interval between updates', type=int)

option_parser.add_option('--threads', dest='threads', metavar='INT', help='number of threads', type=int)

option_parser.add_option('--delay-min', dest='delay_min', help='min delay between requests', metavar='SECONDS',
                         type=int)
option_parser.add_option('--delay-max', dest='delay_max', help='max delay between requests', metavar='SECONDS',
                         type=int)

option_parser.set_defaults(
    wait=60,
    threads=3,
    max_pages=1,
    items_per_page=25,
    delay_min=1,
    delay_max=5
)

(options, args) = option_parser.parse_args()

if options.wait < 1:
    options.wait = 1

if options.max_pages < 1:
    options.max_pages = 1

if options.items_per_page < 1:
    options.items_per_page = 1

if options.threads < 2:
    options.threads = 2

if options.delay_min < 1:
    options.delay_min = 1

if options.delay_max < options.delay_min:
    options.delay_max = options.delay_min + 1

queue = deque()
actions = deque()
lock = Lock()


def cast_delay():
    time.sleep(randint(options.delay_min, options.delay_max))


def periodic_update():
    print('Fetching news...')

    fetch_posts(options.max_pages)
    queue.extend([item for item in PostManager.list(options.items_per_page * options.max_pages) if item.new])
    time.sleep(options.wait)

    with lock:
        actions.appendleft(periodic_update)


def instant_fetch_item():
    if len(queue):
        try:
            last = len(queue) == 1

            item = queue.pop()

            print('Fetching post ' + item.id + ' "' + item.title + '" (' + item.author + ')...')

            if item:
                if not last:
                    cast_delay()

                item = get_item(item.id)

                PostManager.update(item)
        except Exception as e:
            print(e)

        with lock:
            actions.appendleft(instant_fetch_item)


def run_thread():
    while (1):
        with lock:
            if len(actions):
                func = actions.pop()

                if func:
                    try:
                        func()
                    except Exception as e:
                        print(e)
                        time.sleep(10)

        time.sleep(1)


def signal_handler(signum, frame):
    print('Signal: ' + signum)
    sys.exit(0)


class Broker:
    _threads = []

    def __init__(self, max_threads: int):
        actions.append(periodic_update)

        for i in range(max_threads):
            actions.append(instant_fetch_item)
            self._threads.append(Thread(target=run_thread))

        for thread in self._threads:
            thread.start()


signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

broker = Broker(options.threads)
