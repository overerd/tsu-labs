import requests
import re
import time
import os

from random import randint

from optparse import OptionParser

usage = 'Usage: %prog [options] url ...'

option_parser = OptionParser(usage=usage, add_help_option=False)

option_parser.add_option('-h', action='help', help='show this message and exit')
option_parser.add_option('-d', action='store_true', dest='debug', help='display debug information')
option_parser.add_option('-q', action='store_true', dest='verbose', help='disable verbose output')

option_parser.add_option('-f', dest='file', help='write results in a file', metavar='FILE', type=str)
option_parser.add_option('-l', dest='level', help='maximum recursion depth', metavar='LEVEL', type=int)
option_parser.add_option('-s', dest='status', help='allowed http response codes', metavar='STATUS', type=str)
option_parser.add_option('--ua', dest='user_agent', help='User-Agent for HTTP requests', metavar='USER_AGENT', type=str)

option_parser.add_option('--delay-min', dest='delay_min', help='min delay', metavar='SECONDS', type=int)
option_parser.add_option('--delay-max', dest='delay_max', help='max delay', metavar='SECONDS', type=int)

option_parser.add_option('--max-requests', dest='requests_max', help='max number of requests', metavar='INT', type=int)

option_parser.set_defaults(
    verbose=False,
    debug=0,
    level=3,
    status='200',
    requests_max=25,
    user_agent='Mozilla/5.0 (Windows NT 10.0; WOW64) '
               'AppleWebKit/537.36 (KHTML, like Gecko) '
               'Chrome/61.0.3163.100 Safari/537.36',
    delay_min=1,
    delay_max=5
)

(options, args) = option_parser.parse_args()

if not len(args):
    option_parser.print_help()
    exit(1)

if options.delay_min < 1:
    options.delay_min = 1

if options.delay_max < options.delay_min:
    options.delay_max = options.delay_min + 1

if options.debug:
    print('---Options & Arguments---')
    print(options)
    print(args)

if options.level < 0:
    options.level = 0

if options.level > 25:
    options.level = 25

if options.requests_max > 0:
    options.requests_max += 1

re_email_simple = re.compile(r'\b(?<=mailto:)[^@\r\n]+@\w+(?:\.\w+)+', flags=re.I)
re_email_obfuscated = re.compile(
    re.sub(
        '[\s]+', '',
        """
        ([a-z0-9_-]+(?:(?:(?:\s*(?:\[dot\]|\(dot\))\s*|\.)|\s+dot\s+)[a-z0-9_-]+)*(?:\+[a-z0-9_-]+)?)
        (?:(?:\s*(?:\[at\]|\(at\))\s*)|\sat\s|@)
        ([^\s\.@]+(?:(?:(?:\s*(?:\[dot\]|\(dot\))\s*)|\s+dot\s+|\.)[^\s"\.]+)+)(?![^\s])
        """
    ),
    flags=re.I | re.U
)

re_content_type = re.compile('text\/([^\s;]+)')
re_host_select = re.compile('http(s?):\/\/([^\/]+)')
re_links = re.compile(r"""<\s*a.+href\s*=\s*("|')(?!mailto:)([^\1]+?)\1[^>]*>[^<]*<\s*\/\s*a\s*>""", flags=re.I | re.U)
re_link_parent = re.compile("""(?:https?:\/\/[^\/]+)(\/(?:[^\.\s\/]+\/)*)""", flags=re.I | re.U)
re_replace_dot = re.compile('\s+dot\s+|\s*(?:\(dot\)|\[dot\])\s*', flags=re.I)

result_emails = []
parsed_links = []

requests_count = options.requests_max

file_mode = 'w'

authorized_status_codes = [int(i) for i in options.status.split(',')]
authorized_types = ['plain', 'html']


def cast_delay():
    wait = randint(options.delay_min, options.delay_max)

    if options.debug:
        print('[DELAY for ' + str(wait) + ']')

    time.sleep(wait)


def build_host(uri):
    match = re_host_select.match(uri)

    if match:
        if options.debug:
            print('[BUILD HOST:] http' + match[1] + ':\/\/' + match[2])

        return [
            re.compile('http' + match[1] + ':\/\/' + match[2] + '(\/(?:[^\s\/]+\/?)*)'),
            str(match[2]),
            'http' + match[1] + '://' + match[2]
        ]

    raise Exception('wrong uri format: ' + uri)


def validate(r, re_host):
    if not re_host.match(url):
        return

    res_type = r.headers.get('content-type')

    match = re_content_type.match(res_type)

    if not match:
        raise UserWarning('unsupported content-type: ' + res_type)

    res_type = match[1]

    if res_type not in authorized_types:
        raise UserWarning('unsupported content-type: text/' + res_type)

    if r.status_code not in authorized_status_codes:
        raise UserWarning('unsupported http response: ' + r.status_code)

    return r.text


def format_link(link, re_host, str_url_parent):
    match = re_host.match(link)

    if match:
        return match[1]

    match = re_host_select.match(link)

    if match:
        return None

    return str_url_parent + link


def find_links(string, re_host, str_base_url, str_url_parent):
    links = [match[2] for match in re_links.finditer(string)]
    links = [format_link(item, re_host, str_url_parent) for item in links]
    links = [str_base_url + item for item in links if item]
    links = [link for link in links if link not in parsed_links]

    return list(links)


def format_email(email):
    return re.sub(re_replace_dot, '.', email)


def find_emails(string):
    emails = []

    emails.extend(list([match[0] for match in re_email_simple.finditer(string)]))
    emails.extend(list([format_email(match[1] + '@' + match[2]) for match in re_email_obfuscated.finditer(string)]))

    return list([email for email in set(emails) if email not in result_emails])


def get_url_parent(url):
    match = re_link_parent.match(url)

    if not match:
        raise UserWarning('wrong match: ' + str(match))

    return match[1]


def operate_url(url, re_host, str_host, str_base_url, level, referer=None):
    global requests_count

    if level > options.level > 0 or requests_count == 1:
        return

    if options.debug | options.verbose:
        print('\nPage URL: "' + url + '"')

	request_headers = {
		'user-agent': options.user_agent
	}

	if referer:
		request_headers['referer'] = referer

    try:
        validate(requests.head(url, headers=request_headers), re_host)
    except UserWarning as w:
        if options.debug | options.verbose:
            print('[Error] ' + str(w))
        return

    requests_count -= 1

    parsed_links.append(url)

    r = requests.get(url, headers=request_headers)

    emails = find_emails(r.text)
    links = find_links(validate(r, re_host), re_host, str_base_url, get_url_parent(url))

    if emails:
        for email in emails:
            if options.debug:
                print('Found: ' + email)
            result_emails.append(email)

    if links:
        for link in links:
            cast_delay()
            operate_url(link, re_host, str_host, str_base_url, level + 1, url)

if options.file:
    if os.path.exists(options.file):
        selected = False

        while not selected:
            action = (
                input(
                    str('File "' + options.file) + '" already exists. [(R)eplace, (A)ppend, (C)ancel] '
                )
            ).lower()

            if action == 'r':
                file_mode = 'w'

                selected = True
            elif action == 'a':
                file_mode = 'a'

                selected = True
            elif action == 'c':
                exit(1)

for url in args:
    build_host(url)

for url in args:
    links = {}

    (re_host, str_host, str_base_url) = build_host(url)

    if options.verbose:
        print('--- Host: ' + str_host + ' ---')

    operate_url(url, re_host, str_host, str_base_url, 1)

if options.debug:
    print('\n')

result_emails.sort()

if options.file:
    if options.verbose:
        print('Writing file "' + options.file + '"...')

    with open(options.file, mode=file_mode) as file:
        file.writelines('\n'.join(list(result_emails)))

    if options.verbose:
        print('Done.')
else:
    if options.verbose:
        print('Emails:')

    for email in result_emails:
        print(email)
