const https = require('https'), process = require('process'), express = require('express'), morgan = require('morgan'), app = express(), fs = require('fs');
const re_datetime = /\d{4}-(?:1[02]|0\d)-(?:[0-2]\d|3[01])T[0-2]\d:[0-5]\d:[0-5]\d/, re_lead_slash = /^([^\/])/;

if (process.argv.length < 3) return console.error('Usage: app.js config.json\n\nError: no input API key');

const app_config = fs.readFileSync(process.argv[2]);

if (!app_config['meetup']) return console.error('"meetup" not found in CONFIG_FILE');
if (!app_config['eventbrite']) return console.error('"eventbrite" not found in CONFIG_FILE');

const
    SERVER_HOST = app_config['HOST'] ? app_config['HOST'] : '127.0.0.1',
    SERVER_PORT = app_config['PORT'] ? app_config['PORT'] : 8080;

const build_request_url = (server, request, params) => `${server}${request.replace(re_lead_slash, '/$1')}?${Object.entries(params).map((_) => { return _.join('=') }).join('&')}`;
const build_meetup_request_url = (params) => build_request_url('https://api.meetup.com', '/find/upcoming_events', params);
const build_eventbrite_request_url = (request, params) => build_request_url('https://www.eventbriteapi.com/', '/v3/events/search/', params);
const build_google_address2geoposition_url = (address) => build_request_url('https://maps.googleapis.com/', '/maps/api/geocode/xml', {address:address.replace(/[\s\t]/g, '+'), key: app_config['google']['key']});

const storage = new require('./storage.js')(app_config, build_google_address2geoposition_url);

const parse_response = (params, from, to, res, handler) => {
    const body = [];

    delete params['key'];

    if (from) params['from'] = from;
    if (to) params['to'] = to;

    res.on('error', (err) => rej(err));
    res.on('data', (chunk) => body.push(chunk));
    res.on('end', handler)
};

const get_from_meetup = (params, from, to) => {
    return new Promise((res, rej) => {
        https.get(build_meetup_request_url(params), (response) => {
            parse_response(params, from, to, response, () => {
                const data = JSON.parse(body.join(''));

                data['events'].forEach((item) => {
                    let address;

                    if (item.venue) {
                        address = `${item.venue.localized_country_name}, ${item.venue.city} ${item.venue.address_1} (${item.venue.name})`
                    } else if (item.group) {
                        address = `${item.group.localized_location}, ${item.group.name})`
                    } else {
                        address = ''
                    }

                    item['full_address'] = address
                });

                if (!data['events']) {
                    return rej(`Empty items\n\n${JSON.stringify(data['events'])}`)
                }

                res(params, data['events'])
            })
        });
    })
};

const get_from_eventbrite = (params, from, to) => {
    return new Promise((res, rej) => {
        https.get(
            {
                'url': build_eventbrite_request_url(params),
                'headers': { 'Content-Type': 'application/json' }
            },
            (response) => {
                parse_response(params, from, to, response, () => {
                    const data = JSON.parse(body.join(''));

                    data['events'].forEach((item) => {
                        let address;

                        if (item.venue) {
                            if (item.venue.address) {
                                address = `${item.venue.address.country}, ${item.venue.address.localized_address_display}, ${item.venue.name})`
                            } else {
                                address = `${item.venue.name})`
                            }

                            item.lat = item.venue.latitude;
                            item.lon = item.venue.longitude
                        } else {
                            address = ''
                        }

                        item['full_address'] = address
                    });

                    if (!params['items']) {
                        rej(`Empty items\n\n${JSON.stringify(data['events'])}`)
                    }

                    res(params, data['events'])
                })
            });
    })
};

const get_list = (req, res, from, to) => {
    if (from) if (!re_datetime.test(from)) from = undefined;
    if (to) if (!re_datetime.test(to)) to = undefined;

    Promise.all([get_from_meetup({...app_config['meetup']}, from, to), get_from_eventbrite({...app_config['eventbrite']}, from, to)])
        .then((items) => {
            storage.addMany(items)
                .then(() => {
                    return storage.find(from, to, app_config['LIMIT'] ? app_config['LIMIT'] : 10)
                }).then((params, items) => {
                    params['items'] = items;

                    res.render('list', items)
                }).catch((err) => {
                    res.render('error', {'code': res.statusCode = 500, 'msg': err})
                });
        }).catch((err) => {
            res.render('error', {'code': res.statusCode = 500, 'msg': err})
        });
};

app.set('view engine', 'pug');
app.set('views', './templates');

app.use('/', express.static([app_config['ROOT_DIR'] ? app_config['ROOT_DIR'] : __dirname, 'static'].join('/')));
app.use(morgan('dev'));

app.get('/', (req, res) => { get_list(req, res, req.query.from, req.query.to) });
app.get('/:from/:to', (req, res) => { get_list(req, res, req.params.from, req.params.to) });

app.listen(SERVER_PORT, SERVER_HOST, () => { console.log(`Server listening on ${SERVER_HOST}:${SERVER_PORT}...\n`) });
