class PostNotFound(Exception):
    pass


class PostAlreadyExists(Exception):
    pass


class Post:
    def __init__(self, id: str):
        self._id = id
        self.title = ''
        self.content = ''
        self.image = ''
        self.author = ''
        self.link = ''
        self.time = ''

    @property
    def id(self):
        return self._id


def singleton(cls):
    instances = {}

    def instance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]
    return instance


@singleton
class PostStorage:
    def __init__(self):
        self._posts = []
        self._posts_index = {}

    @property
    def posts(self):
        return self._posts

    @property
    def index(self):
        return self._posts_index


class PostManager:
    def __init__(self):
        self._posts = []
        self._posts_index = {}

    @property
    def posts(self):
        return self._posts

    @property
    def index(self):
        return self._posts_index

    @staticmethod
    def add(post):
        db = PostStorage()

        if post.id in db.index:
            raise PostAlreadyExists(post.id)

        db.posts.append(post)
        db.index[post.id] = post

        return post

    @staticmethod
    def exists(id: str):
        db = PostStorage()

        return id in db.index

    @staticmethod
    def get(id: str):
        db = PostStorage()

        if id not in db.index:
            raise PostNotFound(id)

        return db.index[id]

    @staticmethod
    def remove(id: str):
        db = PostStorage()

        if id not in db.index:
            raise PostNotFound(id)

        post = db.index[id]

        db.posts.remove(post)

        del db.index[id]

        return post

    @staticmethod
    def update(post: Post):
        db = PostStorage()

        if post.id not in db.index:
            raise PostNotFound(post.id)

        db.posts[db.posts.index(post)] = post
        db.index[post.id] = post

        return post

    @staticmethod
    def list(index_to: int, index_from: int = 0):
        db = PostStorage()

        if index_from < 0:
            index_from = 0

        if index_to < index_from:
            index_to = index_from

        return db.posts[index_from:index_to]

    @staticmethod
    def get_last():
        db = PostStorage()

        if not db.posts:
            raise PostNotFound('empty')

        return db.posts[-1]