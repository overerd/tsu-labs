const sqlite3 = require('sqlite3'), w2v = require('word2vector'), https = require('https');

const db = new sqlite3.Database(':memory:');

let url_google_address_check;

const selectMany = (db, from, to, limit) => {
    return new Promise((res, rej) => {
        let where = [];

        if (from) where.push(`timestamp >= ${from}`);
        if (to) where.push(`timestamp <= ${to}`);

        where = where.length ? where.join(' AND ') : '';

        db.get(`SELECT * FROM items ${where} LIMIT ${limit};`, (err, obj) => {
            if (err) return rej(err);

            const items = res(obj);

            if (items.length > 0) {
                res(items)
            } else {
                rej('no items found')
            }
        })
    })
};

const selectOne = (db, id) => {
    return new Promise((res, rej) => {
        db.get(`SELECT * FROM items WHERE rowId = ${id} LIMIT 1;`, (err, obj) => {
            if (err) return rej(err);

            const items = res(obj);

            if (items.length > 0) {
                res(items[0])
            } else {
                rej('no items found')
            }
        })
    })
};

const insertion = (db) => {
    const p = db.prepare('INSERT INTO items VALUES (?, ?, ?, ?, ?, ?, ?, ?)');

    return (data) => {
        return new Promise((res, rej) => {
            db.get('SELECT rowId as id FROM items ORDER BY rowId DESC LIMIT 1;', (err, obj) => {
                if (err) rej(err);
                else res(
                    p.run(
                        obj.id + 1,
                        data.lat,
                        data.lon,
                        data.vector,
                        data.timestamp,
                        data.address,
                        data.title,
                        data.content
                    )
                )
            })
        })
    }
};

const address2latlng = (address, return_full_address) => {
    return new Promise((res, rej) => {
        https.get(url_google_address_check(address), (response) => {
            const body = [];

            response.on('error', (err) => rej(err));
            response.on('data', (chunk) => body.push(chunk));
            response.on('end', () => {
                const data = JSON.parse(body.join(''));

                if (data['status'] !== 'OK') {
                    return rej(data['status'])
                }

                const result = data['results'];

                if (return_full_address) {
                    res(
                        [result['geometry']['location']['lat'], result['geometry']['location']['lng']],
                        result['geometry']['formatted_address']
                    )
                } else {
                    res([result['geometry']['location']['lat'], result['geometry']['location']['lng']])
                }
            })
        })
    })
};

const address_distance = async (a, b) => {
    return vectors_distance(await address2latlng(a), await address2latlng(b))
};

const vectors_distance = (a, b) => {
    let sum = 0;

    for (let i = 0; i < a.length; i++) {
        sum += Math.abs(a[i] - b[i])
    }

    return sum
};

const nearSelection = (db, lat_delta, lon_delta, time_delta, max_vect_distance, max_addr_distance) => {
    return (lat, lon, timestamp, vector, address) => {
        return new Promise((resolve, reject) => {
            let results = [];
            let promises = [];

            db.each(
                `SELECT id, vector, address
                FROM items
                WHERE
                    abs(lat - ${lat}) <= ${lat_delta}
                    AND abs(lon - ${lon}) <= ${lon_delta}
                    AND abs(timestamp - ${timestamp}) <= ${time_delta}
                ;`,
                (err, obj) => {
                    if (err) return rej(err);

                    const vect_distance = vectors_distance(obj.vector, vector);

                    address_distance(obj.address, address).then((distance) => {
                        if (vect_distance <= max_vect_distance && distance <= max_addr_distance) {
                            promises.push(new Promise(r => r(results.push(obj))));
                        }
                    }).catch((err) => { reject(err) });
                },
                () => {
                    Promise.all(promises).then((results) => { resolve(results) }).catch((err) => reject(err));
                }
            )
        })
    }
};

const StorageObject = (conf, url_google_address_check_function) => {
    const db = this.db = new sqlite3.Database(conf['database']);

    w2v.load(conf['word2vec_corpus']);

    this.conf = conf;

    url_google_address_check = url_google_address_check_function;

    db.serialize(() => {
        db.run(`CREATE TABLE IF NOT EXISTS items (
                    id INT,
                    lat DOUBLE,
                    lon DOUBLE,
                    vector JSON,
                    timestamp INTEGER,
                    address VARCHAR(4096),
                    title VARCHAR(1024),
                    content TEXT
                );`
        )
    })
};

StorageObject.prototype = {
    'add': async (data) => {
        const conf = this.conf;
        const db = this.db;

        const insert = insertion(db);
        const findNear = nearSelection(
            db,
            conf.lat_delta,
            conf.lon_delta,
            conf.time_delta,
            conf.max_vector_distance,
            conf.max_addr_distance
        );

        const items = findNear(data.lat, data.lon, data.timestamp, w2v.getVector(data.title), address);

        if (items.length > 0) {
            const item = await selectOne(db, id);

            updateItem(data, item)
        } else {
            insert(data)
        }

        insert(data)
    },

    'addMany': (items) => {
        const p = new Promise((r) => { r() });

        for (let i = 0; i < items.length; i++) {
            p.then(() => {
                return this.add(items[i])
            })
        }

        return p
    },

    'find': async (from, to, limit) => {
        return selectMany(this.db, from, to, limit)
    }
};

module.exports = StorageObject;
