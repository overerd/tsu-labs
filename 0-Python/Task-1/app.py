import requests
import re
import sys

args = sys.argv[1:]

if not len(args):
    print('No input')

re_email = re.compile(r'\b[a-z\d_-]+(?:\.[a-z\d_-]+)*(?:\+[a-z\d_-]+)?@[^\s\b]+', re.IGNORECASE)
re_content_type = re.compile(r'text\/([^\s;]+)')

emails = {}

authorized_status_codes = [200]
authorized_types = ['plain', 'html']

request_headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) '
                  'AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/61.0.3163.100 Safari/537.36'
}


def validate(r):
    res_type = r.headers.get('content-type')

    match = re_content_type.match(res_type)

    if not match:
        raise UserWarning('unsupported content-type: ' + res_type)

    res_type = match[1]

    if res_type not in authorized_types:
        raise UserWarning('unsupported content-type: text/' + res_type)

    if r.status_code not in authorized_status_codes:
        raise UserWarning('unsupported http response: ' + r.status_code)

for url in args:
    try:
        validate(requests.head(url, headers=request_headers))

        r = requests.get(url, headers=request_headers)

        validate(r)

    except UserWarning as w:
        print(' [error]: ')
        print(w)

    finally:
        matches = re_email.findall(r.text)

        if matches:
            for match in matches:
                if match not in emails.keys():
                    emails[match] = 1

for email in emails.keys():
    print(email)
