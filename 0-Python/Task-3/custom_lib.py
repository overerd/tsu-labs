from re import sub, compile
from bs4 import BeautifulSoup
from requests import head, get
from custom_db import Post, PostManager

request_headers = {
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) '
                  'AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/61.0.3163.100 Safari/537.36'
}

re_id = compile('(?<=\/comments\/)([^\/]+)(?=\/)')
re_title_replace = compile('\[N\]\s+')


def get_list(after_id: str = None):
    url = 'https://www.reddit.com/r/MachineLearning/search?'

    result = []

    params = {
        'sort': 'new',
        'restrict_sr': 'on',
        'q': 'flair:News'
    }

    if after_id:
        params['after'] = 't3_' + after_id

    r = get(url, params=params, headers=request_headers)

    parser = BeautifulSoup(r.text, 'html.parser')

    items = parser.select('div.search-result')

    last_post_id = None

    for item in items:
        title = item.select_one('header a[href]')

        match = re_id.findall(title['href'])

        if not match:
            print('ID match error: ' + title['href'])
            continue

        link = item.select_one('dev.search-result-footer a[href]')
        content = item.select_one('div.search-result-body div.md')
        author = item.select_one('span.search-author a[href]')
        image = item.select_one('a.thumbnail img')
        time = item.select_one('span.search-time time')

        post = Post(match[0])

        post.title = sub(re_title_replace, '', title.string)
        post.author = author.string
        post.time = time['title']

        if link:
            post.link = link['href']

        if content:
            post.content = content.get_text()

        if image:
            post.image = 'http:' + image['src']

        if not PostManager.exists(post.id):
            result.append(post)

        last_post_id = post.id

    return [result, last_post_id]


def fetch_posts(max_pages: int, items_per_page: int = 25):
    if max_pages <= 0:
        return

    while max_pages > 0:
        (items, last_post_id) = get_list()

        if not items:
            return

        for item in items:
            if not PostManager.exists(item.id):
                PostManager.add(item)

        if len(items) < items_per_page:
            return

        max_pages -= 1
