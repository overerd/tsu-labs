from custom_lib import fetch_posts
from custom_db import PostManager

from optparse import OptionParser

usage = 'Usage: %prog [options]'

option_parser = OptionParser(usage=usage, add_help_option=False)

option_parser.add_option('-h', action='help', help='show this message and exit')

option_parser.add_option('--max-pages', dest='max_pages', metavar='INT', help='max number of pages to parse', type=int)
option_parser.add_option('--items-per-page', dest='items_per_page', metavar='INT', help='items per page', type=int)

option_parser.set_defaults(
    max_pages=1,
    items_per_page=25
)

(options, args) = option_parser.parse_args()

if options.max_pages < 1:
    options.max_pages = 1

if options.items_per_page < 1:
    options.items_per_page = 1

fetch_posts(options.max_pages)

for item in PostManager.list(50):
    print('\nID: ' + item.id)
    print('Title: ' + item.title)
    print('Time: ' + item.time)
    print('Author: ' + item.author)

    if item.image:
        print('Image: ' + item.image)

    if item.content:
        print('---Content\n' + item.content + '---EndOfContent')
