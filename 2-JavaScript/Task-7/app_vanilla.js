const http = require('http');
const https = require('https');
const process = require('process');
const querystring = require('querystring');
const fs = require('fs');

const pug = require('pug');

const re_content_type = /application\/([^;\s]+)/i;
const re_path_replacer = /\/\/+/;
const re_lead_slash = /^([^\/])/;
const re_datetime = /\d{4}-(?:1[02]|0\d)-(?:[0-2]\d|3[01])T[0-2]\d:[0-5]\d:[0-5]\d/;
const re_index = /^\/[^\/]*/;
const re_get_params = /[^?]+\?/;

const re_content_type_json = /application\/json/i;

const usage = () => {
    console.log('Usage: app.js API_KEY [SERVER_ROOT_DIR]\n');
};

const invoke_error = (msg, code) => {
    code = code ? code : 1;

    usage();
    console.error(msg);
    process.exit(code);
};

const dir_path = (items) => items.join('/').replace(re_path_replacer, '/');

const check_existance = (path) => { if (!fs.existsSync(path)) { invoke_error(`directory "${path}" does not exist`) } };
const check_dir_existance = (path, target) => { check_existance(path); if (!fs.lstatSync(path).isDirectory()) { invoke_error(`"${target}" must be a directory (${path})`) } };

if (process.argv.length < 3) {
    invoke_error('no input API key');
}

const API_KEY = process.argv[2];

const app_config = {
    'lat': 35.596922,
    'lon': 139.349335,
    'radius': 20,
    'topic_category': 292,
    'key': API_KEY
};

const SERVER_HOST = '127.0.0.1';
const SERVER_PORT = 8080;

const SERVER_ROOT = process.argv.length > 3 ? process.argv[3] : __dirname;

check_dir_existance(SERVER_ROOT, 'SERVER_ROOT');

check_dir_existance(dir_path([SERVER_ROOT, 'static']), 'static');
check_dir_existance(dir_path([SERVER_ROOT, 'templates']), 'templates');

check_existance(dir_path([SERVER_ROOT, 'templates', 'list.pug']));
check_existance(dir_path([SERVER_ROOT, 'templates', 'error.pug']));

const render_content = pug.compileFile(dir_path([SERVER_ROOT, 'templates', 'list.pug']));
const render_error = pug.compileFile(dir_path([SERVER_ROOT, 'templates', 'error.pug']));

const render_index = (req, res) => { re_index.test(req.url) ? get_list(req, res, ) : send_error(req, res, 404, 'page not found') };

const build_request_url = (request, params) => `https://api.meetup.com${request.replace(re_lead_slash, '/$1')}?${Object.entries(params).map((_) => { return _.join('=') }).join('&') }`;
const parse_get_params = (url) => JSON.parse(['{', url.replace(re_get_params, '').split('&').map((_) => { _ = _.split('='); return _.length > 1 ? `"${_[0]}": "${_[1]}"` : _[0] ? `"${_[0]}": null` : '' }).join(','), '}'].join(''));

const get_json = (url) => {
    return new Promise((resolve, reject) => {
        https.get(url, (res) => {
            res.on('error', (err) => { reject(err) });

            const {statusCode} = res;

            if (statusCode !== 200) {
                return reject(`response code: ${statusCode}`);
            }

            const contentType = res.headers['content-type'];

            let matches = re_content_type.exec(contentType);

            if (matches) {
                if (matches[1] === 'json') {
                    let body = [];

                    res.on('data', (chunk) => body.push(chunk));
                    res.on('end', () => resolve(JSON.parse(body.join(''))));

                    return;
                }
            }

            reject(`wrong content-type header: "${contentType}"`);
        });
    });
};

const send_error = (req, res, code, err) => {
    res.statusCode = code ? code : 500;

    console.log(` [!] ${res.statusCode} ${req.method} ${req.url}`);

    if (re_content_type_json.test(req.headers['accept'])) {
        res.end(JSON.stringify({'error': 1, 'msg': err}));
    } else {
        res.end(render_error({ 'code': code, 'msg': err.toString() }));
    }
};

const send_response = (req, res, code, items, from, to) => {
    res.statusCode = code ? code : 200;

    console.log(` [+] ${res.statusCode} ${req.method} ${req.url}`);

    if (re_content_type_json.test(req.headers['accept'])) {
        res.end(JSON.stringify({'success': 1, 'data': content}));
    } else {
        try {
            res.end(render_content({
                'items': items,
                'lon': app_config.lon,
                'lat': app_config.lat,
                'radius': app_config.radius,
                'from': from,
                'to': to
            }));
        } catch(err) {
            send_error(req, res, 500, err);
        }
    }
};

const get_list = (req, res) => {
    const api_params = {...app_config};
    const get_params = parse_get_params(req.url);

    let from = get_params.from;
    let to = get_params.to;

    if (from) {
        from = querystring.unescape(from);

        if (!re_datetime.test(from)) {
            return send_error(req, res, 400, `wrong "from" format (${from})`);
        }

        api_params['start_date_range'] = from;
    }

    if (to) {
        to = querystring.unescape(to);

        if (!re_datetime.test(to)) {
            return send_error(req, res, 400, `wrong "to" format (${to})`);
        }

        api_params['end_date_range'] = to;
    }

    const url = build_request_url('/find/upcoming_events', api_params);

    get_json(url)
        .then((json) => {
            if (json['events']) {
                json['events'].forEach((item) => {
                    let address;

                    if (item.venue) {
                        address = `${item.venue.localized_country_name}, ${item.venue.city} ${item.venue.address_1} (${item.venue.name})`;
                    } else if (item.group) {
                        address = `${item.group.localized_location}, ${item.group.name})`;
                    } else {
                        address = '';
                    }

                    item.full_address = address;
                });

                send_response(req, res, 200, json['events'], from, to);
            } else {
                send_error(req, res, 500, `Error: empty items\n\n${JSON.stringify(json)}`)
            }
        })
        .catch((err) => {
            send_error(req, res, 500, err)
        })
    ;
};

const server = http.createServer((req, res) => {
    const file_name = dir_path([SERVER_ROOT, 'static', req.url]);

    if (fs.existsSync(file_name)) {
        const stats = fs.lstatSync(file_name);

        if (stats.isDirectory()) {
            return render_index(req, res);
        }

        const stream = fs.createReadStream(file_name);

        stream.on('end', () => {
            console.log(` [-] ${res.statusCode} ${req.method} ${req.url}`);
            res.end();
        });

        stream.on('error', (err) => {
            send_error(req, res, 500, err);
        });

        stream.on('data', (data) => {
            res.write(data)
        });
    } else {
        render_index(req, res);
    }
});

server.listen(SERVER_PORT, SERVER_HOST, () => {
    console.log(`Application Config: ${JSON.stringify(app_config)}`);
    console.log(`\nServer running at ${SERVER_HOST}:${SERVER_PORT}...`);
});
